// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Task.h"
#include "Containers/Map.h"
#include "UObject/NoExportTypes.h"
#include "Kismet/GameplayStatics.h"
#include "TaskManager.generated.h"

UCLASS()
class UAIBase : public UObject
{
	GENERATED_BODY()
public:

};

/**
 * @brief Generic task manager class.
 * @details Shall contain task-related logic for example, binding running tasks to their owners.
 */
UCLASS()
class PROTOTYPE_API UTaskManager : public UObject
{
	GENERATED_BODY()

public:
	/** @return Pointer to the manager's instance. */
	static UTaskManager* Get();
	
	/** @return If task is free, the given instance will be returned, otherwise - a clone of this task. */
	UTask* Clone(UTask* From);

private:
	//Manager's instance.
	static UTaskManager* Instance;
	
	/** @brief Multiply owners can use the same instance of task, so we use TMultiMap. */
	TMultiMap<UTask* /** Task */, UAIBase* /** TaskOwner */ > TaskMap; //Task map don't access in blueprint
};

template<class T>
T* GetGameIntsnace()
{
	return UGameplayStatics::GetGameInstance(this); nullptr; //UKismetSystemLibrary::GetGame //Cast<>()
}

inline UTaskManager* UTaskManager::Get()
{
	//UGameInstance::GetWorld()
	//GetWorld().
	auto GameInstance = UGameplayStatics::GetGameInstance(UObject::StaticClass());
	UKismetSystemLibrary::PrintString(UObject::StaticClass(), GameInstance ? GameInstance->GetName() : "None");
	/*if (!Instance)
	{
		Instance = NewObject<UTaskManager>();
	}*/

	//return Instance;
	return nullptr;
}

inline UTask* UTaskManager::Clone(UTask* From)
{
	UTask* To = nullptr;
	TArray<UAIBase*> Owners;
	TaskMap.MultiFind(From, Owners);

	if (Owners.Num() == 0)
	{
		To = From;
	}
	else
	{
		To = Cast<UTask>(From->Clone());
	}

	return To;
}